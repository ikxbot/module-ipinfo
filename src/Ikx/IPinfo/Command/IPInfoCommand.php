<?php
namespace Ikx\IPinfo\Command;

use Ikx\Core\Command\AbstractCommand;
use Ikx\Core\Command\CommandInterface;
use Ikx\Core\Utils\MessagingTrait;

class IPInfoCommand extends AbstractCommand implements CommandInterface {
    use MessagingTrait;

    public function run()
    {
        $ipAddress = $this->params[0] ?? null;
        if (!$ipAddress) {
            $this->msg($this->channel, __("Error: please specify an IP-address"));
        } else {
            $ipParts = explode('.', $ipAddress);
            $ipParts = array_reverse($ipParts);

            $this->msg($this->channel, sprintf("%s.origin.asn.cymru.com", implode('.', $ipParts)));
            $InfoRecord = dns_get_record(sprintf("%s.origin.asn.cymru.com", implode('.', $ipParts)), DNS_TXT);
            $this->msg($this->channel, __("\x02Information for IP address %s", $ipAddress));

            $hasInfo = false;
            if ($InfoRecord && isset($InfoRecord[0]['txt'])) {
                $hasInfo = true;

                $txt = $InfoRecord[0]['entries'][0];
                $parts = explode(' | ', $txt);

                $this->msg($this->channel, __("\x02AS:\x02 %s / \x02Country:\x02 %s", $parts[0], $parts[2]));
            }

            if(!$hasInfo) {
                $this->msg($this->channel, "No information found.");
            }
        }
    }

    public function describe()
    {
        return __("Fetch information for a given IP address");
    }
}